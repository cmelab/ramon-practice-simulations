import hoomd
import hoomd.md
from cme_utils.manip.convert_rigid import init_wrapper

hoomd.context.initialize("")

steps = 2e8
in_file = "mol/asp_box.hoomdxml"
in_file = "mol/asp_tails_box.hoomdxml"


system = init_wrapper(in_file)
hoomd.dump.gsd("test_init.gsd", group=hoomd.group.all(), period=None, overwrite=True)

nl = hoomd.md.nlist.cell()
lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)

lj.set_params(mode="xplor")
lj.pair_coeff.set(
    system.particles.types,
    [
        i
        for (i, v) in zip(
            system.particles.types, [_.startswith("_R") for _ in system.particles.types]
        )
        if v
    ],
    epsilon=0.0,
    sigma=0.0,
    r_cut=False,
)

lj.pair_coeff.set("C", "C", epsilon=.1, sigma=1)

integrator_mode = hoomd.md.integrate.mode_standard(dt=0.00001)

rigid = hoomd.group.rigid_center()
# flex = hoomd.group.nonrigid()
# both = hoomd.group.union('both', rigid, flex)

intergator = hoomd.md.integrate.langevin(
    group=rigid, kT=hoomd.variant.linear_interp(points=[(0, 20.0), (2e7, 1.0)]), seed=42
)

# harmonic = hoomd.md.bond.harmonic()
# harmonic.bond_coeff.set('C-C', k=375, r0=0.55)

hoomd.analyze.log(
    "log/{}-asp.log".format("anneal-1500"),
    quantities=[
        "potential_energy",
        "temperature",
        "volume",
        "kinetic_energy",
        "pressure",
        "pair_lj_energy",
    ],
    period=2e5,
    overwrite=True,
)
hoomd.dump.gsd(
    "traj/{}-asp.gsd".format("anneal"),
    period=2e6,
    group=hoomd.group.all(),
    overwrite=True,
)
hoomd.run(1e5)
integrator_mode.set_params(dt=0.0001)
hoomd.run(5e5)
integrator_mode.set_params(dt=0.001)


hoomd.run(steps)
