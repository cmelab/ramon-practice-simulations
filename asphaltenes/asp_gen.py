import mbuild as mb
import sys
import numpy as np

class Asphaltene1(mb.Compound):
    def __init__(self):
        super(Asphaltene1, self).__init__()
        carbon = mb.Particle(name='C')
        self.add(mb.Particle(name='C', pos=[-1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-0.0, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[9.68, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[9.68, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[10.89, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[10.89, 0.7, 0]), label='CR[$]')
        for atom in self:
            for atom1 in self:
                if self.min_periodic_distance(atom.xyz, atom1.xyz) < 1.5:
                    if atom1 == atom:
                        continue
                    self.add_bond((atom1, atom))

        for atom in self:
            atom.xyz /= 10
        self.label_rigid_bodies()
        self.add(mb.Port(anchor=self[27]), label='tail0')
        self['tail0'].translate([0.154/1.5, 0, 0])
        self.add(mb.Port(anchor=self[0]), label='tail1')
        self['tail1'].translate([-0.154/1.5, 0, 0])


class Asphaltene2(mb.Compound):
    def __init__(self):
        super(Asphaltene2, self).__init__()
        carbon = mb.Particle(name='C')
        self.add(mb.Particle(name='C', pos=[-1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-0.0, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 2.8, 0]), label='CR[$]')
        for atom in self:
            for atom1 in self:
                if self.min_periodic_distance(atom.xyz, atom1.xyz) < 1.5:
                    if atom1 == atom:
                        continue
                    self.add_bond((atom1, atom))
        for atom in self:
            atom.xyz /= 10
        self.label_rigid_bodies()



class Asphaltene_3(mb.Compound):
    def __init__(self):
        super(Asphaltene_3, self).__init__()
        self.add(mb.Particle(name='C', pos=[-2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-0.0, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 2.8, 0]), label='CR[$]')
        for atom in self:
            for atom1 in self:
                if self.min_periodic_distance(atom.xyz, atom1.xyz) < 1.5:
                    if atom1 == atom:
                        continue
                    self.add_bond((atom1, atom))

        for atom in self:
            atom.xyz /= 10
        self.label_rigid_bodies()
        self.add(mb.Port(anchor=self[21]), label='tail0')
        self['tail0'].translate([0.154/2, 0, 0])
        self.add(mb.Port(anchor=self[1]), label='tail1')
        self['tail1'].translate([-0.154/2, 0, 0])



class Asphaltene4(mb.Compound):
    def __init__(self):
        super(Asphaltene4, self).__init__()
        carbon = mb.Particle(name='C')
        self.add(mb.Particle(name='C', pos=[-2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 9.1, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-0.0, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 9.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 9.1, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 9.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 9.1, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.259999999999999, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, 4.8999999999999995, 0]), label='CR[$]')
        for atom in self:
            for atom1 in self:
                if self.min_periodic_distance(atom.xyz, atom1.xyz) < 1.5:
                    if atom1 == atom:
                        continue
                    self.add_bond((atom1, atom))

        for atom in self:
            atom.xyz /= 10
        self.label_rigid_bodies()
        self.add(mb.Port(anchor=self[0]), label='tail0')
        self['tail0'].translate([-0.154/2, 0, 0])
        self.add(mb.Port(anchor=self[50]), label='tail1')
        self['tail1'].translate([0.154/2, 0, 0])

class Asphaltene5(mb.Compound):
    def __init__(self):
        super(Asphaltene5, self).__init__()
        carbon = mb.Particle(name='C')
        self.add(mb.Particle(name='C', pos=[-3.63, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-3.63, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-0.0, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.259999999999999, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, 4.8999999999999995, 0]), label='CR[$]')
        for atom in self:
            for atom1 in self:
                if self.min_periodic_distance(atom.xyz, atom1.xyz) < 1.5:
                    if atom1 == atom:
                        continue
                    self.add_bond((atom1, atom))

        for atom in self:
            atom.xyz /= 10
        self.label_rigid_bodies()
        self.add(mb.Port(anchor=self[38]), label='tail0')
        self['tail0'].translate([0.154/2, 0, 0])
        self.add(mb.Port(anchor=self[1]), label='tail1')
        self['tail1'].translate([-0.154/2, 0, 0])

class Asphaltene6(mb.Compound):
    def __init__(self):
        super(Asphaltene6, self).__init__()
        carbon = mb.Particle(name='C')
        self.add(mb.Particle(name='C', pos=[-2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-0.0, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 4.8999999999999995, 0]), label='CR[$]')
        for atom in self:
            for atom1 in self:
                if self.min_periodic_distance(atom.xyz, atom1.xyz) < 1.5:
                    if atom1 == atom:
                        continue
                    self.add_bond((atom1, atom))

        for atom in self:
            atom.xyz /= 10
        self.label_rigid_bodies()
        self.add(mb.Port(anchor=self[1]), label='tail0')
        self['tail0'].translate([-0.154/1.5, 0, 0])
        self.add(mb.Port(anchor=self[25]), label='tail1')
        self['tail1'].translate([0.154/2, 0.154/2, 0])
        self.add(mb.Port(anchor=self[18]), label='tail2')
        self['tail2'].translate([0.154/2, -0.154/2, 0])

class Asphaltene7(mb.Compound):
    def __init__(self):
        super(Asphaltene7, self).__init__()
        carbon = mb.Particle(name='C')
        self.add(mb.Particle(name='C', pos=[-3.63, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-3.63, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-0.0, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.259999999999999, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, 4.8999999999999995, 0]), label='CR[$]')
        for atom in self:
            for atom1 in self:
                if self.min_periodic_distance(atom.xyz, atom1.xyz) < 1.5:
                    if atom1 == atom:
                        continue
                    self.add_bond((atom1, atom))

        for atom in self:
            atom.xyz /= 10
        self.label_rigid_bodies()
        self.add(mb.Port(anchor=self[0]), label='tail0')
        self['tail0'].translate([-0.154/1.5, 0, 0])
        self.add(mb.Port(anchor=self[44]), label='tail1')
        self['tail1'].translate([0.154/2, 0.154/2, 0])
        self.add(mb.Port(anchor=self[21]), label='tail2')
        self['tail2'].translate([0.154/2, -0.154/2, 0])

class Asphaltene8(mb.Compound):
    def __init__(self):
        super(Asphaltene8, self).__init__()
        carbon = mb.Particle(name='C')
        self.add(mb.Particle(name='C', pos=[-2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 9.1, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-0.0, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 9.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 9.1, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 9.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 9.1, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[9.68, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[9.68, 2.8, 0]), label='CR[$]')
        for atom in self:
            for atom1 in self:
                if self.min_periodic_distance(atom.xyz, atom1.xyz) < 1.5:
                    if atom1 == atom:
                        continue
                    self.add_bond((atom1, atom))

        for atom in self:
            atom.xyz /= 10
        self.label_rigid_bodies()

class Asphaltene9(mb.Compound):
    def __init__(self):
        super(Asphaltene9, self).__init__()
        carbon = mb.Particle(name='C')
        self.add(mb.Particle(name='C', pos=[-2.42, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 9.1, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-0.0, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 9.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 9.1, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 9.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 9.1, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.839999999999999, 9.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 11.2, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.049999999999999, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 9.1, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.050000000000001, 11.899999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.259999999999999, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, 11.2, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.260000000000001, 9.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, 9.1, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, 11.899999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.47, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[9.68, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[9.68, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[9.68, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[9.68, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[9.68, 9.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[9.68, 11.2, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[10.889999999999999, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[10.89, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[10.89, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[10.89, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[10.89, 9.1, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[12.099999999999998, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[12.1, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[12.1, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[12.1, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[13.309999999999999, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[13.309999999999999, 4.8999999999999995, 0]), label='CR[$]')
        for atom in self:
            for atom1 in self:
                if self.min_periodic_distance(atom.xyz, atom1.xyz) < 1.5:
                    if atom1 == atom:
                        continue
                    self.add_bond((atom1, atom))

        for atom in self:
            atom.xyz /= 10
        self.label_rigid_bodies()
        self.add(mb.Port(anchor=self[0]), label='tail0')
        self['tail0'].translate([-0.154/2, 0, 0])
        self.add(mb.Port(anchor=self[60]), label='tail1')
        self['tail1'].translate([-0.154/2, -0.154/2, 0])

class Asphaltene10(mb.Compound):
    def __init__(self):
        super(Asphaltene10, self).__init__()
        carbon = mb.Particle(name='C')
        self.add(mb.Particle(name='C', pos=[-3.63, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-3.63, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-2.42, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-1.21, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, -1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[-0.0, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[0.0, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, -0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[1.21, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[2.42, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[3.63, 7.699999999999999, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[4.84, 7.0, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 0.7, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[6.05, 4.8999999999999995, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.259999999999999, 5.6, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, 1.4, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[7.26, 2.8, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, 3.5, 0]), label='CR[$]')
        self.add(mb.Particle(name='C', pos=[8.469999999999999, 4.8999999999999995, 0]), label='CR[$]')
        for atom in self:
            for atom1 in self:
                if self.min_periodic_distance(atom.xyz, atom1.xyz) < 1.5:
                    if atom1 == atom:
                        continue
                    self.add_bond((atom1, atom))

        for atom in self:
            atom.xyz /= 10
        # add tail to index 1
        self.label_rigid_bodies()
        self.add(mb.Port(anchor=self[1]), label='tail0')
        self['tail0'].translate([-0.154/2, 0, 0])
        self.add(mb.Port(anchor=self[38]), label='tail1')
        self['tail1'].translate([0.154/2, 0, 0])


class Asp10_Tail(mb.Compound):
    def __init__(self):
        super(Asp10_Tail, self).__init__()
        tail0 = Alkane(chain_length=9)
        tail1 = Alkane(chain_length=7)
        asp = Asphaltene10()
        self.add(tail0)
        self.add(tail1)
        self.add(asp)
        mb.force_overlap(move_this=tail0,
                             from_positions=tail0['up'],
                             to_positions=asp["tail0"])
        mb.force_overlap(move_this=tail1,
                             from_positions=tail1['up'],
                             to_positions=asp["tail1"])

class Asp9_Tail(mb.Compound):
    def __init__(self):
        super(Asp9_Tail, self).__init__()
        tail0 = Alkane(chain_length=6)
        tail1 = Alkane(chain_length=9)
        asp = Asphaltene9()
        self.add(tail0)
        self.add(tail1)
        self.add(asp)
        mb.force_overlap(move_this=tail0,
                             from_positions=tail0['up'],
                             to_positions=asp["tail0"])
        mb.force_overlap(move_this=tail1,
                             from_positions=tail1['up'],
                             to_positions=asp["tail1"])

class Asp7_Tail(mb.Compound):
    def __init__(self):
        super(Asp7_Tail, self).__init__()
        tail0 = Alkane(chain_length=7)
        tail1 = Alkane(chain_length=8)
        tail2 = Alkane(chain_length=5)
        asp = Asphaltene7()
        self.add(tail0)
        self.add(tail1)
        self.add(tail2)
        self.add(asp)
        mb.force_overlap(move_this=tail0,
                             from_positions=tail0['up'],
                             to_positions=asp["tail0"])
        mb.force_overlap(move_this=tail1,
                             from_positions=tail1['up'],
                             to_positions=asp["tail1"])
        mb.force_overlap(move_this=tail2,
                             from_positions=tail2['up'],
                             to_positions=asp["tail2"])

class Asp6_Tail(mb.Compound):
    def __init__(self):
        super(Asp6_Tail, self).__init__()
        tail0 = Alkane(chain_length=5)
        tail1 = Alkane(chain_length=13)
        tail2 = Alkane(chain_length=6)
        asp = Asphaltene6()
        self.add(tail0)
        self.add(tail1)
        self.add(tail2)
        self.add(asp)
        mb.force_overlap(move_this=tail0,
                             from_positions=tail0['up'],
                             to_positions=asp["tail0"])
        mb.force_overlap(move_this=tail1,
                             from_positions=tail1['up'],
                             to_positions=asp["tail1"])
        mb.force_overlap(move_this=tail2,
                             from_positions=tail2['up'],
                             to_positions=asp["tail2"])

class Asp5_Tail(mb.Compound):
    def __init__(self):
        super(Asp5_Tail, self).__init__()
        tail0 = Alkane(chain_length=6)
        tail1 = Alkane(chain_length=9)
        asp = Asphaltene5()
        self.add(tail0)
        self.add(tail1)
        self.add(asp)
        mb.force_overlap(move_this=tail0,
                             from_positions=tail0['up'],
                             to_positions=asp["tail0"])
        mb.force_overlap(move_this=tail1,
                             from_positions=tail1['up'],
                             to_positions=asp["tail1"])

class Asp4_Tail(mb.Compound):
    def __init__(self):
        super(Asp4_Tail, self).__init__()
        tail0 = Alkane(chain_length=7)
        tail1 = Alkane(chain_length=2)
        asp = Asphaltene4()
        self.add(tail0)
        self.add(tail1)
        self.add(asp)
        mb.force_overlap(move_this=tail0,
                             from_positions=tail0['up'],
                             to_positions=asp["tail0"])
        mb.force_overlap(move_this=tail1,
                             from_positions=tail1['up'],
                             to_positions=asp["tail1"])

class Asp3_Tail(mb.Compound):
    def __init__(self):
        super(Asp3_Tail, self).__init__()
        tail0 = Alkane(chain_length=2)
        tail1 = Alkane(chain_length=9)
        asp = Asphaltene_3()
        self.add(tail0)
        self.add(tail1)
        self.add(asp)
        mb.force_overlap(move_this=tail0,
                             from_positions=tail0['up'],
                             to_positions=asp["tail0"])
        mb.force_overlap(move_this=tail1,
                             from_positions=tail1['up'],
                             to_positions=asp["tail1"])

class Asp1_Tail(mb.Compound):
    def __init__(self):
        super(Asp1_Tail, self).__init__()
        tail0 = Alkane(chain_length=1)
        tail1 = Alkane(chain_length=4)
        asp = Asphaltene1()
        self.add(tail0)
        self.add(tail1)
        self.add(asp)
        mb.force_overlap(move_this=tail0,
                             from_positions=tail0['up'],
                             to_positions=asp["tail0"])
        mb.force_overlap(move_this=tail1,
                             from_positions=tail1['up'],
                             to_positions=asp["tail1"])

class Tail_mon(mb.Compound):
    def __init__(self):
        super(Tail_mon, self).__init__()
        self.add(mb.Particle(name='C', pos=[0,0,0]), label='C[$]')
        self.add(mb.Port(anchor=self[0]), label='up')
        self.add(mb.Port(anchor=self[0]), label='down')
        self['up'].translate([0, -0.154/2, 0])
        self['down'].translate([0, 0.154/2, 0])


class Alkane(mb.Compound):
    def __init__(self, chain_length):
        super(Alkane, self).__init__()

        last_unit = Tail_mon()
        self.add(last_unit)
        self.add(last_unit["up"], "up", containment=False)
        for _ in range(chain_length - 1):
            current_unit = Tail_mon()
            mb.force_overlap(move_this=current_unit,
                             from_positions=current_unit['up'],
                             to_positions=last_unit['down'])
            self.add(current_unit)
            last_unit=current_unit
            self.add(current_unit['down'], 'down', containment=False)

asp1 = Asphaltene1()
#asp2 = Asphaltene2()
asp_3 = Asphaltene_3()
asp4 = Asphaltene4()
asp5 = Asphaltene5()
asp6 = Asphaltene6()
asp7 = Asphaltene7()
#asp8 = Asphaltene8()
asp9 = Asphaltene9()
asp10 = Asphaltene10()
asp1.save('mol/asp1.hoomdxml', ref_mass=12.0107, overwrite=True)
#asp2.save('mol/asp2.hoomdxml', ref_mass=12.0107, overwrite=True)
asp_3.save('mol/asp3.hoomdxml', ref_mass=12.0107, overwrite=True)
asp4.save('mol/asp4.hoomdxml', ref_mass=12.0107, overwrite=True)
asp5.save('mol/asp5.hoomdxml', ref_mass=12.0107, overwrite=True)
asp6.save('mol/asp6.hoomdxml', ref_mass=12.0107, overwrite=True)
asp7.save('mol/asp7.hoomdxml', ref_mass=12.0107, overwrite=True)
#asp8.save('mol/asp8.hoomdxml', ref_mass=12.0107, overwrite=True)
asp9.save('mol/asp9.hoomdxml', ref_mass=12.0107, overwrite=True)
asp10.save('mol/asp10.hoomdxml', ref_mass=12.0107, overwrite=True)
# 200 = 64200
# 400 = 128400
# 3200 = 1027200
#filled_box = mb.packing.fill_box(compound=[asp1, asp_3, asp4, asp5, asp6, asp7, asp9, asp10],
#                                 #density=50.0,
#                                 box = [32.5, 32.5, 32.5],
#                                 n_compounds=[125]*8,
#                                 fix_orientation=[True]*8,
#                                 edge=0.2,
#                                 overlap=0.1)
#filled_box.save('mol/asp_box.hoomdxml', overwrite=True, ref_mass=12.0107, ref_distance=3.75, shift_coords=True)

asp_tails = [Asp1_Tail(), Asp3_Tail(), Asp4_Tail(), Asp5_Tail(), Asp6_Tail(), Asp7_Tail(), Asp9_Tail(), Asp10_Tail()]
filled_box = mb.packing.fill_box(compound=asp_tails,
                                 #density=50.0,
                                 box = [32.5, 32.5, 32.5],
                                 n_compounds=[125]*8,
                                 fix_orientation=[True]*8,
                                 edge=0.2,
                                 overlap=0.1)
filled_box.save('mol/asp_tails_box.hoomdxml', overwrite=True, ref_mass=12.0107, ref_distance=3.75, shift_coords=True)
