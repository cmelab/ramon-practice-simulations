#!/bin/bash -l
#BATCH -p batch
#SBATCH -J asp_sims
#SBATCH -o progress-%J.log
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mail-user=rm2258@nau.edu
#SBATCH --mail-type=FAIL
#SBATCH -t 2-00:00:00
#SBATCH --gres=gpu:1

python -u asp_gen.py
