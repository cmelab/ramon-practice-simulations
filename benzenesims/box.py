import mbuild as mb
import sys

N=int(sys.argv[1]) # Specify the amount of particles you want in the simulation box

benzene = mb.load('../moleculelib/benzene.mol2')
benzene.label_rigid_bodies()
filled_box = mb.packing.fill_box(benzene, density=876.5, n_compounds=N, fix_orientation=True)
filled_box.save('benzene_box.hoomdxml', overwrite=True, ref_distance=2.8, ref_mass=12.017)
