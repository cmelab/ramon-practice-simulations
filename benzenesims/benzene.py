import hoomd
import hoomd.md
from cme_utils.manip.convert_rigid import init_wrapper
import sys
hoomd.context.initialize("")

steps = 1000
T = int(sys.argv[1]) # Spicify the temp 

system = init_wrapper('benzene_box.hoomdxml')

nl = hoomd.md.nlist.tree()
lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)

lj.set_params(mode='xplor')
lj.pair_coeff.set(system.particles.types, [i for (i, v) in zip(system.particles.types, [_.startswith("_R") for _ in system.particles.types]) if v], epsilon=0.0, sigma=0.0, r_cut=0)
lj.pair_coeff.set('C','C', epsilon=1, sigma=1)
hoomd.md.integrate.mode_standard(dt=0.005)

intergator = hoomd.md.integrate.langevin(group=hoomd.group.rigid_center(), kT=T, seed=42)
T = str(T)
hoomd.analyze.log(filename=T+"-bt-output.log",
                    quantities=['potential_energy',
                                'temperature'],
                    period=100,
                    overwrite=True)
hoomd.dump.gsd(T+"-bt.gsd",
                period=10,
                group=hoomd.group.all(),
                overwrite=True)

hoomd.run(steps)
