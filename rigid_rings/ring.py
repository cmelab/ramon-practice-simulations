import hoomd
import hoomd.md
hoomd.context.initialize("")

steps = 3e5
#sets the number of time steps
T = 1
#sets the kT

uc = hoomd.lattice.unitcell(N = 1,
                            a1 = [6, 0, 0],
                            a2 = [0, 6, 0],
                            a3 = [0, 0, 7],
                            dimensions = 3,
                            type_name= ['R'],
                            moment_inertia = [[21, 28, 49]]) #change the moi here onece you get the values from the moi.py

system = hoomd.init.create_lattice(unitcell=uc, n=[2,4,2])
system.particles.types.add('A')
rigid = hoomd.md.constrain.rigid()
rigid.set_param('R',
                types=['A']*6,
                positions=[(-1,0.5,0), (0,1,0), (1,0.5,0),
                (-1,-0.5,0), (0,-1,0), (1,-0.5,0)])

rigid.create_bodies()
nl = hoomd.md.nlist.cell()
lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)

lj.set_params(mode='xplor')
lj.pair_coeff.set(['R', 'A'], ['R', 'A'], epsilon=1.0, sigma=1.0)
hoomd.md.integrate.mode_standard(dt=0.005)

rigid =hoomd.group.rigid_center()
hoomd.md.integrate.langevin(group=rigid, kT=T, seed=42)
hoomd.analyze.log(filename="ring2-output.log",
                    quantities=['potential_energy',
                                'temperature'],
                    period=100,
                    overwrite=True)
hoomd.dump.gsd("ring2.gsd",
                period=100,
                group=hoomd.group.all(),
                overwrite=True)

hoomd.run(steps)
