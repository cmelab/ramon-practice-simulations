import numpy

def moit(points, masses): #TODO: Currently assumes center of mass is at origin, which is not currently checked, so moments will be wrong.
    '''Moment of Inertia Tensor
    Assumes center of mass is at origin
    Assumes 3xN array for points, 1xN for masses'''
    m = masses  # Makes things look nicer
    x = points[:, 0]
    y = points[:, 1]
    z = points[:, 2]
    I_xx = numpy.sum((y**2 + z**2)*m)
    I_yy = numpy.sum((x**2 + z**2)*m)
    I_zz = numpy.sum((x**2 + y**2)*m)
    return numpy.array((I_xx,I_yy,I_zz))

points = numpy.array([[0, 0, 0], [-1, 0.5, 0], [0, 1, 0], [1, 0.5, 0], [-1, -0.5, 0],[0, -1, 0], [1, -0.5, 0]]) # update the positions of the particles here

masses = numpy.array([[1], [1], [1], [1], [1], [1], [1]]) # update the masses of the particles here

moi = moit(points, masses)
print(moi)
