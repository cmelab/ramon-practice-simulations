import numpy
import matplotlib.pyplot
import sys

# Used to make plots of potential energy (PE) and temperature. By the way these will be separate graphs.

f = str(sys.argv[1])
cut = int(sys.argv[2])

# Add the file name above and quantity

data = numpy.genfromtxt('rigid_rings/log/ring2-output.log', skip_header=True)
data1 = numpy.genfromtxt('asphaltenes/fry_logs/log/anneal-asp.log', skip_header=True)
fig = matplotlib.pyplot.figure(figsize=(8,4.4), dpi=140)
axes1 = fig.add_subplot(1, 1, 1)

x, y = (data[:, 0], data1[:, 0]), (data[:, 1], data1[:, 1])
axes1.plot(x[cut:], y[cut:])
axes1.set_xlabel('time step')
axes1.set_ylabel('Potential Energy')

matplotlib.pyplot.show()
