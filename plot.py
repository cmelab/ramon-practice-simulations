import numpy
import matplotlib.pyplot
import sys

# Used to make plots of potential energy (PE) and temperature. By the way these will be separate graphs.

f = str(sys.argv[1])
cut = int(sys.argv[2])
q = 'potential_energy'
# Add the file name above and quantity

data = numpy.genfromtxt(f, skip_header=True)
print('average potential energy',numpy.mean(data[:,1]))
print('average temperature', numpy.mean(data[:,2]))

fig = matplotlib.pyplot.figure(figsize=(8,4.4), dpi=140)
axes1 = fig.add_subplot(1, 2.2, 1)
axes2 = fig.add_subplot(1, 2.2, 2)

x, y = data[:, 0], data[:, 1]
axes1.plot(x[cut:], y[cut:])
axes1.set_xlabel('time step')
axes1.set_ylabel(q)

x, y = data[:, 0], data[:, 2]
axes2.plot(x[cut:], y[cut:])
axes2.set_xlabel('time step')
axes2.set_ylabel('temperature')
        
matplotlib.pyplot.show()
