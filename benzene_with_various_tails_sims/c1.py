import hoomd
import hoomd.md
from cme_utils.manip.convert_rigid import init_wrapper
import sys
hoomd.context.initialize("")

steps = 1e8

system = init_wrapper('mol/bt1_box.hoomdxml', restart_rigid=True)

nl = hoomd.md.nlist.tree()
lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)

lj.set_params(mode='xplor')
lj.pair_coeff.set(system.particles.types, [i for (i, v) in zip(system.particles.types, [_.startswith("_R") for _ in system.particles.types]) if v], epsilon=0.0, sigma=0.0, r_cut=0)
lj.pair_coeff.set('C','C', epsilon=1, sigma=1)
#hoomd.dump.gsd('int.gsd', period=None, group=hoomd.group.all(), overwrite=True)
hoomd.md.integrate.mode_standard(dt=0.005)

rigid = hoomd.group.rigid_center()
flex = hoomd.group.nonrigid()
both = hoomd.group.union('both', rigid, flex)

intergator = hoomd.md.integrate.langevin(group=both, kT= hoomd.variant.linear_interp(points = [(0, 8.0), (1e8, 1.0)]), seed=42)
harmonic = hoomd.md.bond.harmonic()
harmonic.bond_coeff.set('C-C', k=10, r0=0.55)
hoomd.analyze.log("log/c1/{}-bt.log".format(str('anealing')),
                    quantities=['potential_energy',
                                'temperature',
				'TPS',
				'volume',
			        'kinetic_energy',
			        'pressure',
			        'pair_lj_energy',
			        'bond_harmonic_energy',
			        'dihedral_harmonic_energy'],
                    period=100000,
                    overwrite=True)
hoomd.dump.gsd("trajectories/c1/{}-bt.gsd".format(str('anealing')),
                period=5000000,
                group=hoomd.group.all(),
                overwrite=True)
hoomd.deprecated.dump.xml(filename="rigid_center_flex.xml", 
                          group=both,
		          position=True, 
                          image=True, mass=True, diameter=True, type=True, body=True,
                          orientation=True, inertia=True, restart=False, period=5e6)

hoomd.run_upto(2e8)
