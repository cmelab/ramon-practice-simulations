import mbuild as mb
import sys

# what about a benzene with any tail?
# Ramon (responds with)-Why not!!!
# You will need the carbon.mol2 and benzene.mol2 molecule files

n_lengthoftail = int(sys.argv[1]) # specify the length of the tail
N_particles =int(sys.argv[2]) # specify the amount of particles you want

class Benzene(mb.Compound):
    def __init__(self):
        super(Benzene, self).__init__()
        mb.load('../moleculelib/benzene.mol2', compound=self)
        self.label_rigid_bodies()
        mb.translate(self, -self[0].pos)
        port = mb.Port(anchor=self[0])
        self.add(port, label='A')
                            
class Tail(mb.Compound):
    def __init__(self):
        super(Tail, self).__init__()
        mb.load('../moleculelib/carbon.mol2', compound=self)
        mb.translate(self, -self[0].pos)
        port = mb.Port(anchor=self[0])
        self.add(port, label='A')
        port = mb.Port(anchor=self[0])
        self.add(port, label='B')
        mb.translate(self['A'], [0.14, -0.07, 0])
        mb.translate(self['B'], [-0.07, 0.07, 0])

class Bt(mb.Compound):
    def __init__(self, tail):
        super(Bt, self).__init__()
        self.add(Benzene(), label='benzene')
        self.add(tail, label='tail')
        mb.force_overlap(move_this=self['benzene'],
                from_positions=self['benzene']['A'],
                to_positions=self['tail']['A'])

if __name__ == "__main__":
    if n_lengthoftail == 0:
        mycomp = Benzene()
    else:
        tail = mb.Polymer(Tail(), n_lengthoftail, port_labels=('A', 'B'))
        tail.energy_minimization()
        mycomp = Bt(tail)

    
    mycomp.save('mol/bt{}.hoomdxml'.format(str(n_lengthoftail)), overwrite=True) # This is the molecule file if this checks out the box should be fine
    
    filled_box = mb.packing.fill_box(mycomp, density=862.0, n_compounds=N_particles, fix_orientation=True) # This is where the box is packed
    filled_box.save('mol/bt{}_box.hoomdxml'.format(str(n_lengthoftail)), overwrite=True, ref_distance=2.8, ref_mass=12.017) # The box gets saved at this point
