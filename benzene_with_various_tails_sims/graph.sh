#!/bin/bash -l
#BATCH -p batch
#SBATCH -J graph 
#SBATCH -o progress_graph-%J.log
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mail-user=rm2258@nau.edu
#SBATCH --mail-type=FAIL
#SBATCH -t 2-00:00:00
#SBATCH --gres=gpu:1

python -u benzenexvarioustails.py 0 1
python -u tails.py 
python -u benzenexvarioustails.py 0 10
python -u tails.py 
python -u benzenexvarioustails.py 0 100
python -u tails.py
python -u benzenexvarioustails.py 0 1000
python -u tails.py 
python -u benzenexvarioustails.py 0 10000
python -u tails.py 
python -u benzenexvarioustails.py 0 100000
python -u tails.py 
python -u benzenexvarioustails.py 0 1000000
python -u tails.py 
