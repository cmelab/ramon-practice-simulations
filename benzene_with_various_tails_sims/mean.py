import numpy as np
import matplotlib.pyplot as plt
import sys

plt.rcParams['figure.autolayout'] = True
plt.rcParams['savefig.facecolor'] = 'none'  # Sets the background for plot white, transparent on axis
plt.rcParams['axes.linewidth'] = 2.5
plt.rcParams['lines.linewidth'] = 3.0

plt.rcParams['xtick.major.size'] = 8
plt.rcParams['ytick.major.size'] = 8
plt.rcParams['xtick.minor.size'] = 5
plt.rcParams['ytick.minor.size'] = 5
plt.rcParams['xtick.major.pad'] = 10
plt.rcParams['ytick.major.pad'] = 10
plt.rcParams['xtick.labelsize'] = 20
plt.rcParams['ytick.labelsize'] = 20
plt.rcParams['xtick.major.width'] = 1.5
plt.rcParams['ytick.major.width'] = 1.5
plt.rcParams['xtick.minor.width'] = 1.5
plt.rcParams['ytick.minor.width'] = 1.5
plt.rcParams['lines.markeredgewidth'] = 1.5
plt.rcParams['lines.solid_capstyle'] = 'round'

a = np.array([304.462, 
             304.377,
             303.824,
             302.479,
             306.182,
             302.108,
             304.679,
             305.682,
             304.97 ,
             304.78]) #10,000 c3 particles


b = np.array([69.0447,
             69.4187,
             69.4923,
             68.9443,
             68.3481,
             69.8202,
             69.6775,
             69.3613,
             69.3993,
             69.2739]) #50,000 c3 particles
             
c = np.array([1928.52,
              1923.02,
              1934.09,
              1926.36,
              1935.13,
              1929.53,
              1929.94,
              1926.33,
              1916.07,
              1924.47]) #1,000 c3 particles

d = np.array([639.436,
              635.707,
              652.958,
              658.178,
              653.49,
              656.477,
              652.374,
              653.848,
              652.879,
              648.759]) #5,000 c3 particles

f = np.array([
             ])

fig = plt.figure(figsize=(4,2.2), dpi=300)
axes1 = fig.add_subplot(1, 1, 1)

x, y, yerr = ([1000 * 9, 5000 * 9, 10000 * 9, 50000 * 9]), ([c.mean(), d.mean(), a.mean(), b.mean()]), ([c.std(), d.std(), a.std(), b.std()])

axes1.errorbar(x, y, yerr=yerr, marker='x', markersize=8)
axes1.set_xscale("log", nonposx='clip')
axes1.set_yscale("log", nonposy='clip')
axes1.set_xlabel('N (atoms)')
axes1.set_ylabel('TPS')
plt.savefig("tps.png", dpi=300)
