import hoomd
import hoomd.md
from cme_utils.manip.convert_rigid import init_wrapper
import sys
hoomd.context.initialize("")

steps = 1e6
T = 1

File = 'mol/bt0_box.hoomdxml' 


hoomd.context.initialize("")
system = init_wrapper(File)
nl = hoomd.md.nlist.tree()
lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)

lj.set_params(mode='xplor')
lj.pair_coeff.set(system.particles.types, [i for (i, v) in zip(system.particles.types, [_.startswith("_R") for _ in system.particles.types]) if v], epsilon=0.0, sigma=0.0, r_cut=0)
lj.pair_coeff.set('C','C', epsilon=1, sigma=1)
#hoomd.dump.gsd('int.gsd', period=None, group=hoomd.group.all(), overwrite=True)
hoomd.md.integrate.mode_standard(dt=0.005)

rigid = hoomd.group.rigid_center()
flex = hoomd.group.nonrigid()
both = hoomd.group.union('both', rigid, flex)

intergator = hoomd.md.integrate.langevin(group=both, kT=T, seed=42)
harmonic = hoomd.md.bond.harmonic()
harmonic.bond_coeff.set('C-C', k=375, r0=0.55)
hoomd.analyze.log("{}-bt.log".format(str(T)),
                    quantities=['potential_energy',
                                'temperature'],
                    period=1000,
                    overwrite=True)
hoomd.dump.gsd("{}-bt.gsd".format(str(T)),
                period=50000,
                group=hoomd.group.all(),
                overwrite=True)
hoomd.run(steps)
